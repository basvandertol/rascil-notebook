Click [here](https://sdhp.stfc.skao.int/binderhub/v2/gl/basvandertol%2Frascil-notebook/HEAD?urlpath=%2Ftree%2Fnotebooks%2FDP-ART-System-Demo-16.6-Team-Schaap.ipynb) to directly open the demo.
To access the notebook a user should sign in using `<jira-username>@ad.skatelescope.org` as well as the `<jira-password>`. Note than this will create a new server instance, out of a maximum of five servers per user. At the jupyterhub [overview page](https://sdhp.stfc.skao.int/binderhub/jupyterhub/hub/home) one can reconnect to, start, stop and delete servers.


# Introduction

This repository contains the source files for 
the [demo](https://sdhp.stfc.skao.int/binderhub/v2/gl/basvandertol%2Frascil-notebook/HEAD?urlpath=%2Ftree%2Fnotebooks%2FDP-ART-System-Demo-16.6-Team-Schaap.ipynb) given by [Team Schaap](https://confluence.skatelescope.org/display/SE/SCHAAP+Team) at the 
[2022-11-23 DP ART System Demo 16.6](https://confluence.skatelescope.org/display/SE/2022-11-23+DP+ART+System+Demo+16.6) on integrating [DP3](https://git.astron.nl/RD/DP3) steps into [RASCIL]([link](https://gitlab.com/ska-telescope/external/rascil-main)).

Clicking the demo link launches a new server. See the notes at the top of this page on starting and stopping servers.

# Running notebooks on STFC cluster

The [SKA telescope developer portal](https://developer.skao.int/en/latest/index.html) has a [section on BinderHub](https://developer.skao.int/en/latest/tools/binderhub.html) with instructions on how to create a Jupyter server from a repository.

Use BinderHub - <https://k8s.stfc.skao.int/binderhub/> or 
<https://sdhp.stfc.skao.int/binderhub/> to create a server from a repository.

Use JupyterHub - <https://k8s.stfc.skao.int/binderhub/jupyterhub/> or <https://sdhp.stfc.skao.int/binderhub/jupyterhub/> to manage previously created servers.

The k8s and sdhp versions run servers in different environments. The k8s version seems to give a larger working memory, while the sdhp version includes a ~1TB volume at /shared for large shared datasets.

The [user documentation](https://mybinder.readthedocs.io/en/latest/) of Binder describes the various [config files](https://mybinder.readthedocs.io/en/latest/using/config_files.html#config-files) to create an environment. The most flexible (though not recommended) way is through  a [Dockerfile](https://mybinder.readthedocs.io/en/latest/tutorials/dockerfile.html)

It appears that in the sdhp environment, when the docker image is build by running the Dockerfile, gitlab.com is not visible, even though the repository containing the Dockerfile can be on gitlab.

As a workaround the root Dockerfile in this repository is just a single line importing a prebuild image from docker.io.

The Dockerfile to prebuild this image is docker/Dockerfile.

# Building the Notebook Environment

Preparation
```
git clone git@gitlab.com:basvandertol/rascil-notebook.git
cd rascil-notebook
docker build -t svandertol/my-image:0.7 docker
docker push svandertol/my-image:0.7
```

Now fill out the fields at <https://sdhp.stfc.skao.int/binderhub/> like this

![](binder_screenshot.png)

Do not forget to select GitLab.com in the checkbox and URL in the File/URL checkbox. The URL (not fully shows in the image) is `/tree/notebooks/DP-ART-System-Demo-16.6-Team-Schaap.ipynb`

# RISE

RISE turns a classical jupyter notebook into an interactive slide show. It does not work in the newer jupyter lab environment. The classical notebook can be accessed on a jupyterlab server by using a just `/tree` in the url instead of `/lab/tree`.

# Python-casacore

Ubuntu 22.04 comes with casacore package that installs casacore version 3.4.0 including the python bindings python-casacore. 

The requirements for rascil-main insist on python-casacore version 3.5.2. Installing the requirements for rascil-main installs this versions next to version 3.4.0.
Since dp3 also links againt casacore, two versions of casacore are loaded when using dp3 and rascil together in python. This results in a crash when opening an MS.

The solution was to fork rascil-main and lower the requirement for python-casacore to 3.4.

# Python path

DP3 installs it's python files in a `site-packages` sub directory. The default python path only includes the `dist-packages` sub directories. For now the python path is prepended in the python notebook. 
```
import sys
sys.path.insert(0, "/usr/local/lib/python3.10/site-packages")
```
Alternatively the path could be prepended at the entrypoint of the docker image

# WEIGHT vs WEIGHT_SPECTRUM column

When rascil reads an MS it uses the WEIGHT column to fill a WEIGHT_SPECTRUM like datastructure by expanding along the frequency axis.

To force the same weights for a MS read by DP3 and rascil the WEIGHT_SPECTRUM column is set to all ones using taql
```
taql "UPDATE tDDECal.MS SET WEIGHT_SPECTRUM=1.0"
```

